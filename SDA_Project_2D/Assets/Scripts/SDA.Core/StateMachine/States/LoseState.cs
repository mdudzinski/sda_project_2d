﻿using UnityEngine;

namespace SDA.Core
{
    public class LoseState : BaseState
    {
        public override void InitState(GameController gC)
        {
            base.InitState(gC); //wywoływanie bazowej wersji metody
        }

        public override void UpdateState()
        {

        }

        public override void FixedUpdateState()
        {

        }

        public override void DestroyState()
        {

        }
    }
}
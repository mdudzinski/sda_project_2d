﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SDA.Core
{
    public class MenuState : BaseState
    {
        public override void InitState(GameController gC)
        {
            base.InitState(gC); //wywoływanie bazowej wersji tej metody

            gC.MenuView.ShowView();

            gC.MenuView.StartButton.onClick.AddListener(() => gC.ChangeState(new GameState()));
            //AddListener - metoda, która pozwoli nam wykonać inną metodę podpiętą pod button
            //" => " wyrażenie lambda - definiujemy metode anonimową(bez nazwy, podajemy tylko część
            //z argumentami, ale że nie może mieć argumentów to zostawiamy puste nawiasy)
        }

        private void GoToGameState()
        {
            gC.ChangeState(new GameState());
        }

        public override void UpdateState()
        {
            
        }

        public override void FixedUpdateState()
        {

        }

        public override void DestroyState()
        {
            gC.MenuView.StartButton.onClick.RemoveAllListeners();
            gC.MenuView.HideView();
        }
    }
}
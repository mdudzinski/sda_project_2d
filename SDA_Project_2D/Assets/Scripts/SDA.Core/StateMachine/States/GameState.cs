﻿using UnityEngine;

namespace SDA.Core
{
    public class GameState : BaseState
    {
        public override void InitState(GameController gC)
        {
            base.InitState(gC); //wywoływanie bazowej wersji metody

            gC.PlayerMovement.Init();
        }

        public override void UpdateState()
        {
            Debug.Log("Menu UPDATE!");
        }

        public override void FixedUpdateState()
        {
            gC.PlayerMovement.UpdatePosition();
        }

        public override void DestroyState()
        {
            gC.PlayerMovement.Dispose();
        }
    }
}
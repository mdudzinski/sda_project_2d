using UnityEngine;

namespace SDA.Core
{

    public abstract class BaseState //abstrakcyjna klasa z zaimplementowanymi metodami
                                    //do stworzenia zainicjowania MachineState
    {
        protected GameController gC; 

        public virtual void InitState(GameController gC)
        {
            this.gC = gC;
        }

        public abstract void UpdateState();

        public abstract void FixedUpdateState();

        public abstract void DestroyState();
    }
}

using UnityEngine;
using SDA.UI;
using SDA.Player;

namespace SDA.Core
{
    public class GameController : MonoBehaviour
    {

        [SerializeField]
        private MenuView menuView; //private - żeby nie można było podmienić, ale
                                   //SerializeField pozwala, żeby się do tego dostać
                                   //z pozycji inspektora

        public MenuView MenuView => menuView;// = {get{return menuView}}
                                             //properties - specjalna zmienna, która
                                             //zanim coś przypisze albo zwróci, pozwala
                                             //coś ze sobą jeszcze zrobić, ktoś kto pobierze
                                             //może się do tego dostać i z tego korzystać,
                                             //ale nie może tego nadpisać na inną zmienną

        [SerializeField]
        private PlayerMovement playerMovement;
        public PlayerMovement PlayerMovement => playerMovement;


        private BaseState currentState;//zmienna która trzyma
                                       //aktualny stan

        void Start()
        {
            // inicjalizacja zmiennych
            //ustawienia startowego stanu
            ChangeState(new MenuState());
        }

        void Update()
        {
            //if(currentState != null)
            //currentState.Update();

            currentState?.UpdateState();//to jest inna forma
                                        //zapisu tego co powyżej
        }

        private void FixedUpdate()
        {
            currentState?.FixedUpdateState(); 
        }

        private void OnDestroy()
        {
            //co ma się stać przed wyłączniem gry -> np save
            ChangeState(null);
        }

        public void ChangeState(BaseState newState)
        {
            currentState?.DestroyState();
            currentState = newState;
            currentState?.InitState(this); //this -> obiekt na którym,
                                           //znajduje się ten skrypt
        }
    }
}

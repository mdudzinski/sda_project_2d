using UnityEngine;
using UnityEngine.UI;

namespace SDA.UI
{
    public abstract class BaseView : MonoBehaviour
    { 
        public virtual void ShowView() //metoda w której implementujemy
                                       //pokazywanie się widoku
        {
            this.gameObject.SetActive(true);
        }

        public virtual void HideView()//metoda w której implementujemy
                                      //ukrywanie się widoku
        {
            this.gameObject.SetActive(false);
        }
    }
}

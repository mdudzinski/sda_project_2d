using UnityEngine;
using UnityEngine.UI;

namespace SDA.UI
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField]
        private Button startButton; // wywołujemy button
        public Button StartButton => startButton; //udostępniamy button do wglądu
                                                  //i korzystania ale nie do zmiany(read only)

        public void ShowView()
        {
            this.gameObject.SetActive(true);
        }

        public void HideView()
        {
            this.gameObject.SetActive(false);
        }
    }
}
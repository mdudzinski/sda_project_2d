using UnityEngine;

namespace SDA.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private const string HORIZONTAL_AXIS = "Horizontal";
        private const string SPRINT_AXIS = "Sprint";
        private const string JUMP_AXIS = "Jump";

        [SerializeField]
        private Rigidbody2D rigidbody2D;

        [SerializeField]
        private float playerSpeed;

        [SerializeField]
        private float sprintMultiplier;
        //sprintMultiplier mnożnik zwiększania prędkości
        //poruszania się Playera co fizyczną klatkę

        [SerializeField]
        private float maxWalkSpeed;

        [SerializeField]
        private float maxRunSpeed;

        [SerializeField]
        private float jumpMultiplier;

        public void Init()
        {
            //ustawia startową pozycję, prędkość, etc
            gameObject.SetActive(true);

            //rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void UpdatePosition()
        {
            //określa prędkość spowalniania do zatrzymania
            //playera po puszczeniu przycisku na klawiaturze
            //określa wartość -1 czy 1 - kierunek lewo - prawo
            float playerInput = Input.GetAxisRaw(HORIZONTAL_AXIS);
            float sprintInput = Input.GetAxisRaw(SPRINT_AXIS);
            float jumpInput = Input.GetAxisRaw(JUMP_AXIS);

            //jeśli gracz się nie rusza to gracz zatrzymuje się w miejscu
            if (playerInput == 0)
                rigidbody2D.velocity = Vector2.zero;

            //finalMultiplier - określa jak dużo prędkości Player
            //dostaje co fizyczną klatkę
            float finalMultiplier = playerInput * playerSpeed;

            if (sprintInput > 0)
            {

                finalMultiplier *= sprintMultiplier;
                //to samo co: finalMultiplier = finalMultiplier * sprintMultiplier)
            }

            rigidbody2D.AddForce(Vector2.right * finalMultiplier, ForceMode2D.Force);

            //wartość x to wartość jak szybko poruszamy się w lewo albo w prawo
            var x = rigidbody2D.velocity.x;

            //określamy kierunek, sprawdzamy jaka jest prędkość za pomocą Sign
            var direction = Mathf.Sign(x);

            //Abs - Absolute Value - wartość bezwzględna odległość na osi współrzędnych,
            //określa jaką mamy prędkość, niezależnie od kierunku
            //(nie ważne czy -1 [lewo] czy +1 [prawo]) 
            x = Mathf.Abs(x);


            if (sprintInput == 0)
            {
                //Clamp(x, 0, 3) to Clamp sprawi, że x = 3; zamyka liczbę w podanych
                //w nawiasie brzegach
                x = Mathf.Clamp(x, 0, maxWalkSpeed);
            }
            else
            {
                x = Mathf.Clamp(x, 0, maxRunSpeed);
            }
            Vector2 finalVelocity = new Vector2(x * direction, rigidbody2D.velocity.y);

            //wektor prędkości przypisany do Rigidbody2D
            rigidbody2D.velocity = finalVelocity;


            RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, Vector2.down, 0.2f);

            if (raycastHit.collider != null)
            {
                rigidbody2D.AddForce(Vector2.right * finalMultiplier, ForceMode2D.Force);
            }


            //if(Input.GetKey(KeyCode.A))
            //{
            //    //wyzerowanie wyhamowywania od razu po naciśnięciu
            //    //przycisku poruszania w drugą stronę
            //    if (rigidbody2D.velocity.x > 0)
            //        rigidbody2D.velocity = Vector2.zero;


            //    rigidbody2D.AddForce(Vector2.left * 10, ForceMode2D.Force);
            //    //w nawiasie oznaczamy kierunek * prędkość,
            //    //nadanie prędkości( stałej lub przyśpieszenia)
            //}

            //if(Input.GetKey(KeyCode.D))
            //{
            //    //wyzerowanie wyhamowywania od razu po naciśnięciu
            //    //przycisku poruszania w drugą stronę
            //    if (rigidbody2D.velocity.x < 0)
            //        rigidbody2D.velocity = Vector2.zero;

            //    rigidbody2D.AddForce(Vector2.right * 10, ForceMode2D.Force);
            //    //w nawiasie oznaczamy kierunek * prędkość,
            //    //nadanie prędkości( stałej lub przyśpieszenia)
            //}
        }

        public void Dispose()
        {
            gameObject.SetActive(false);
        }
    }
}
